#ifndef TERMINAL_H
#define TERMINAL_H

#ifdef _MSC_VER
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif
#include <libssh/libssh.h>

extern void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
extern Uint32 getpixel(SDL_Surface *surface, int x, int y);

class Terminal
{
    public:
        Terminal(SDL_Window *window);
        virtual ~Terminal();
        void twrite(unsigned char *c, int len);
        SDL_Surface *getSurface();
        void drawChar(unsigned char c);
        void setZmodem(int *z);
        char *gettext(int sx, int sy, int ex, int ey);
        void clickLink(int x, int y);
        void scrollback();
        void sb_up();
        void sb_down();
        void screenshot();
        void toggleIceColours();
        int isIceEnabled();

    protected:
    private:
        void scrollUp();
        SDL_Window *win;
        SDL_Surface *screen;
        SDL_Surface *font_bmp;
        SDL_Surface *pot_noodle_bmp;
        SDL_Surface *topaz_plus_bmp;
        SDL_Surface *mk_plus_bmp;
        SDL_Surface *mk_bmp;
        SDL_Surface *topaz_bmp;
        SDL_Surface *mosoul_bmp;
        SDL_Surface *screenbacker;
        SDL_Surface *screencopy;
        SDL_Surface *blink_screen;
        SDL_Surface *scrollback_screen;
        Uint32 last_show;
        int color;
        int bgcolor;
        int cur_col;
        int cur_row;
		int state;
        int params[16];
        int param_count;
        int save_col;
        int save_row;
        int bold;
        int vt320_count;
        int vt320[16];
        int cursor;
        int zmodem_detect;
        int *zmodem;
        int curfont;
        unsigned char screen_buffer[80][25];
        unsigned char scrollback_buffer[80][100];
        int blinking;
        int blink_toggle;
        int high_intensity_bg;
        int high_intensity_bg_enabled;
        int blink_disabled;
        int scrollingBack;
        int scrollBackTop;
        int icecolours;
        Uint32 colours[16] = { 0xff000000, 0xffaa0000, 0xff00aa00, 0xffaaaa00, 0xff0000aa, 0xffaa00aa, 0xff0055aa, 0xffaaaaaa, 0xff555555, 0xffff5555, 0xff55ff55, 0xffffff55, 0xff5555ff, 0xffff55ff, 0xff55ffff, 0xffffffff };
};



#endif // TERMINAL_H
